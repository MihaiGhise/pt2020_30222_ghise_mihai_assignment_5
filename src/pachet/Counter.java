package pachet;

import java.util.List;
import java.util.Map;

public interface Counter {
	
	Map<String, Integer> counts(List<MonitoredData> list);
	
}
