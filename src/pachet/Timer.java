package pachet;

public class Timer {
	
	private int hour, minute, second;
	private int time=0;
	
	public int toSeconds(String time) {
		return 3600*Integer.parseInt(time.substring(0, 2))+60*Integer.parseInt(time.substring(3, 5))+Integer.parseInt(time.substring(6));	
	}
	
	
	public String difference(String time1, String time2, int differentDays) {
		int difference=0, hourDif, minuteDif, secondDif;
		String hourString = null, minuteString= null, secondString=null;
		if(differentDays==0) {
			difference=toSeconds(time2)-toSeconds(time1);
		}else {
			difference= 3600*24-toSeconds(time1)+toSeconds(time2);
		}
		hourDif = difference / 3600;
		minuteDif = difference%3600 / 60;
		secondDif = difference%3600 %60 ;
		if(hourDif < 10) {
			hourString = "0"+hourDif;
		}
		else {
			hourString = hourDif+"";
		}
		if(minuteDif < 10) {
			minuteString = "0"+minuteDif;
		}
		else {
			minuteString = minuteDif+"";
		}
		if(secondDif < 10) {
			secondString = "0"+secondDif;
		}
		else {
			secondString = secondDif+"";
		}
			return hourString + ":"+minuteString+":"+secondString;
	}
	
	public void add(String time) {
		this.time = this.time + toSeconds(time);
	}
	
	public void toHours() {
		this.hour = time / 3600;
		this.minute = time%3600 / 60;
		this.second = time%3600 %60 ;
	}


	public int getHour() {
		return hour;
	}


	public void setHour(int hour) {
		this.hour = hour;
	}


	public int getMinute() {
		return minute;
	}


	public void setMinute(int minute) {
		this.minute = minute;
	}


	public int getSecond() {
		return second;
	}


	public void setSecond(int second) {
		this.second = second;
	}


	public int getTime() {
		return time;
	}


	public void setTime(int time) {
		this.time = time;
	}
	
}
