package pachet;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.Formatter;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

public class FileWriter {

	private Formatter f;
	
	public void writeMonitoredDataList(List<MonitoredData> monitoredData) {
		
		try {
			f = new Formatter("Task_1.txt");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		for(MonitoredData md : monitoredData) {
			f.format("%s   %s   %s\n", md.getStartTime(), md.getEndTime(), md.getActivity());
		}
		f.close();
	
	}
	
	public void writeActivitiesFrequency(Map<String, Integer> map) {
		try {
			f = new Formatter("Task_3.txt");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		for (Map.Entry<String,Integer> entry : map.entrySet()) { 
            f.format("%s   %d\n", entry.getKey(), entry.getValue());
		}
		f.close();
	}
	
	public void writeActivitiesFrequencyPerDay(Map<Integer, Map<String, Integer>> map) {
		try {
			f = new Formatter("Task_4.txt");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		for (Map.Entry<Integer, Map<String, Integer>> entry : map.entrySet()) { 
            f.format("%d: ", entry.getKey());
            for (Map.Entry<String,Integer> entry1 : entry.getValue().entrySet()) { 
                f.format("%s   %d\n", entry1.getKey(), entry1.getValue());
    		}
            f.format("\n");
		}
		f.close();
	}
	
	public void writeTotalTime(Map<String, Timer> map) {
		try {
			f = new Formatter("Task_5.txt");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		for (Map.Entry<String, Timer> entry : map.entrySet()) { 
            f.format("%s   %s:%s:%s\n", entry.getKey(), entry.getValue().getHour(), entry.getValue().getMinute(), entry.getValue().getSecond()); 
            
		}
		f.close();
	}
	
	public void writeNumber(int number) {
		try {
			f = new Formatter("Task_2.txt");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		f.format("%d", number);
		f.close();
	}
	
}
