package pachet;

import java.util.List;
import java.util.Map;

public interface TotalTime {
	
	Map<String, Timer> countTotal(List<MonitoredData> list);
	
}
