package pachet;

import java.util.List;

public interface Compare {
	
	int countIfEqual(List<MonitoredData> list, int count);
	
}
