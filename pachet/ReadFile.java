package pachet;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public class ReadFile {
	
	List<MonitoredData> monitoredData = new ArrayList<MonitoredData>();
	
	public ReadFile() throws IOException, ParseException{
		
		try (Stream<String> stream = Files.lines(Paths.get("C:\\Users\\Mihai\\Documents\\Activities.txt"))) {
		Function<String[],MonitoredData> factory = data->{
			   DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			   return new MonitoredData(data[0],data[1],separate(data[2]));
			};
			monitoredData = stream.map(line -> line.split("\t\t")).map(factory::apply)  
		             .collect(Collectors.toCollection(ArrayList::new));
			//printBeginTime();
			//count();
			//activitiesFrequency();
			activitiesPerDay();
	} catch (IOException e) {
        e.printStackTrace();
    }
	}
	
	public void count() {
		int count = 0;
		Compare compare = (monitoredDataL, counter) -> {
			List<String> list = new ArrayList<String>();
			String zi = separate(monitoredDataL.get(0).getStartTime());
			for (MonitoredData md1 : monitoredDataL) {
				if (!separate(md1.getStartTime()).equals( zi)) {
					counter++;
				}
				zi = separate(md1.getStartTime());
				list.add(zi);
		}
			for (MonitoredData md1 : monitoredDataL) {
				if (!list.contains(separate(md1.getStartTime()))) {
					counter++;
				}
			}
			System.out.println(counter);
			return counter;
		};
		
		count = compare.countIfEqual(monitoredData, count);
		System.out.println(count);
	}
	
	//numar activitatile 
	public void activitiesFrequency() {
		Counter counter = (monitoredDataLbd) -> {
			Map<String, Integer> map = new HashMap<String, Integer>();
			for(MonitoredData md : monitoredDataLbd) {
				Integer j = map.get(md.getActivity());
				if(j != null) {
					map.put(md.getActivity(), j+1);
				}else {
					map.put(md.getActivity(), 1);
				}
			}
			map.forEach((k,v)->System.out.println("Key: " + k + " Value: " + v)); 
			return map;
		};
		counter.counts(monitoredData);
	}
	
	//aici am numarat fiecare activitate de cate ori se intampla in fiecare zi
	public Map<Integer, Map<String, Integer>> activitiesPerDay() {
		//iau zilele de la prima din lista pana la ultima
		int someDay = monitoredData.get(0).getStartingDay();
		int finalDays = monitoredData.get(monitoredData.size()-1).getFinishDay(); 
		CounterPerDay counter = (day, finalDay) -> {
		Map<Integer, Map<String, Integer>> map = new HashMap<Integer, Map<String, Integer>>();
		//pune mapurile mici in ala mare
		while(day!=finalDay) {
			Map<String, Integer> map1 = new HashMap<String, Integer>();
			for(MonitoredData md : monitoredData) {
				if(md.getFinishDay()==day || md.getStartingDay()==day) {
				Integer j = map1.get(md.getActivity());
				if(j != null) {
					map1.put(md.getActivity(), j+1);
				}else {
					map1.put(md.getActivity(), 1);
				}
			}
			}
			if (map1!=null) {
			//aici afisez mapurile mici si le pun in cel mare
			map1.forEach((k,v)->System.out.println("Key: " + k + " Value: " +v));
			map.put(day, map1);
			}
			day++;
		}
		//map.forEach((k,v)->System.out.println("Key: " + k));
		return map;
		};
		return counter.countPerDay(someDay, finalDays);
	}
	
	public String separate(String time){
		Pattern checkRegex = Pattern.compile("\\S+");
		Matcher regexMatcher = checkRegex.matcher(time);
		
		while (regexMatcher.find()){
			if (regexMatcher.group().length() != 0){
				return regexMatcher.group().trim();
			}
		}
		return null;
	}
	
	public void printBeginTime() {
		monitoredData.forEach((n) -> System.out.println(n.getStartTime()));
	}
}	

