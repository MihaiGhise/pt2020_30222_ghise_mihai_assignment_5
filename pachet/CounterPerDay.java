package pachet;

import java.util.Map;

public interface CounterPerDay {

		Map<Integer, Map<String, Integer>> countPerDay(int day, int finalDay);
}
